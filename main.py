import os
import time

from telegram.ext import MessageHandler, Filters, Updater
import win32print

from setting import approved_chat_ids, tickets_printer, docx_printer
from setting import TOKEN


def print_document(document_path: str, printer) -> None:
    """
        печатает на принтере по умолчанию документ с именем document
    :type document_path: str
    """
    win32print.SetDefaultPrinterW(printer)
    win32print.SetDefaultPrinter(printer)
    os.startfile(document_path, "print")

    return None


def get_extension(filename):
    return os.path.splitext(filename)


def receive_document(update, context):
    chat_id = update.message.chat_id
    if chat_id in approved_chat_ids:
        filename = update.message.document.file_name
        print(filename)
        match get_extension(filename):
            case '.docx':
                update.message.document.get_file().download(custom_path=f'downloaded_docx/{filename}')
                time.sleep(1)
                print_document(f'downloaded_docx/{filename}', docx_printer)
            case '.xlsx':
                update.message.document.get_file().download(custom_path=f'downloaded_docx/{filename}')
                time.sleep(1)
                print_document(f'downloaded_docx/{filename}', tickets_printer)
            case _:
                print("No access")
    else:
        print("No access")


def main() -> None:
    updater = Updater(TOKEN)
    dispatcher = updater.dispatcher
    dispatcher.add_handler(MessageHandler(Filters.document, receive_document))
    updater.start_polling()


if __name__ == '__main__':
    main()
